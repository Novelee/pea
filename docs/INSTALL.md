# PEA Precise Estimation Algorithm

The  PEA Precise Estimation Algorithm supports reading from `RINEX v2` or `RINEX v3` observation files to obtain estimates of the satellite clocks

## Overview

The *Analysis Centre Software (ACS)* is a processing package being developed to processes GNSS observations for geodetic applications.

This document details the software dependencies that need to be installed in order to run the pea and the ancillary programs.

### Dependencies

The following packages need to be installed with the minimum versions as shown below. This guide will outline the preferred method of installation.

    CMAKE  > 3.0 requires openssl-devel to be installed (requires openssl-devel)
    YAML   > 0.6
    Boost  > 1.70
    gcc    > 4.1
    Eigen3

### Build

To build the PEA Precise Estimation Algorithm...

We suggest using the following directory structure when installing the ACS toolkit. It will be created by following this guide.

    /data/
    └── acs/
        ├── pea/
        └── pod/


The following is an example procedure to install the dependencies necessary to run the `pea` on a base ubuntu linux distribution

Update the base operating system:

    $ sudo apt update
	$ sudo apt upgrade

Install base utilities gcc, gfortran, git, openssl, blas, lapack, etc

    $ sudo apt install -y git gobjc gobjc++ gfortran libopenblas-dev openssl curl net-tools openssh-server cmake make \
    liblapack-dev gzip vim libssl1.0-dev python3-cartopy python3-scipy python3-matplotlib python3-mpltoolkits.basemap

Create a temporary directory structure to make the dependencies in:

    $ sudo mkdir -p /data/tmp
    $ cd /data/tmp

##### YAML

We are using the YAML library to parse the configuration files used to run many of the programs found in this library (https://github.com/jbeder/yaml-cpp).
Here is an example of how we have installed the yaml library from source:

    $ cd /data/tmp
    $ sudo git clone https://github.com/jbeder/yaml-cpp.git
    $ cd yaml-cpp
    $ sudo mkdir cmake-build
    $ cd cmake-build
    $ sudo cmake .. -DCMAKE\_INSTALL\_PREFIX=/usr/local/ -DYAML\_CPP\_BUILD\_TESTS=OFF
    $ sudo make install yaml-cpp
    $ cd ../..
    $ sudo rm -fr yaml-cpp

#### Boost

We rely on a number of the utilities provided by boost (https://www.boost.org/), such as their time and logging libraries.

    $ cd /data/tmp/
    $ sudo wget -c https://dl.bintray.com/boostorg/release/1.73.0/source/boost_1_73_0.tar.gz
    $ sudo gunzip boost_1_73_0.tar.gz
    $ sudo tar xvf boost_1_73_0.tar
    $ cd boost_1_73_0/
    $ sudo ./bootstrap.sh
    $ sudo ./b2 install
    $ cd ..
    $ sudo rm -fr boost_1_73_0/ boost_1_73_0.tar

#### Eigen3

Eigen3 is used for performing matrix calculations, and has a very nice API.

    $ cd /data/tmp/
    $ sudo git clone https://gitlab.com/libeigen/eigen.git
    $ cd eigen
    $ sudo mkdir cmake-build
    $ cd cmake-build
    $ sudo cmake ..
    $ sudo make install
    $ cd ../..
    $ sudo rm -rf eigen

### Installing PEA

#### PEA Executable

    $ cd /data/acs/

Clone the repository via https:

    $ git clone https://bitbucket.org/geoscienceaustralia/pea.git 

You should now have...

	pea
	├── INSTALL.md
	├── LICENSE.md
	├── README.md
	├── aws/                - for automated builds in aws
	├── config/
	│   ├── Ex00-UnitTest.yaml
	│   ├── Ex01-PPP.yaml
	│   ├── Ex02-Network.yaml
	│   ├── Ex03-Network_Orbits.yaml
	│   ├── Ex04-Ionosphere.yaml
	│   ├── Ex05-Realtime.yaml
	│   ├── iontest_20115w.yaml
	│   └── PPP-iontest.yaml
	├── cpp/
	│   ├── CMakeLists.txt
	│   ├── cmake           - files to help cmake find dependencies
	│   ├── docs            - automatic code documentation configuration
	│   └── src/
	│       ├── 3rdparty/   - see ACKNOWLEDGEMENTS in README.md
	│       ├── common/     - libraries used by the pea
	│       ├── iono/       - routines for ionosphere modelling
	│       ├── pea/        - main for `pea`
	│       └── rtklib/     - subset of modified routines from RTKlib see ACKNOWLEDGEMENTS in README.md
	└── python
	    ├── config
	    ├── README.md
	    └── source
	        ├── download_examples.py
	        ├── install_examples.py
	        └── other helper programs


Prepare a directory to build in, its better practise to keep this seperated from the source code.

    $ cd pea/cpp
    $ mkdir -p build
    $ cd build

Run cmake to find the build dependencies and create the make file:

    $ cmake ..

Now build the `pea`

    $ cmake --build $PWD --target pea

To change to build location substitue your preferred destination for `$PWD` , e.g `/usr/local/bin`

Alternatively to the command above you can make the code in parallel using:

   $ make -j 5 all

where the -j flag controls how many jobs can be run at the same time.

Check to see if you can execute the `pea`:

    $ ./pea

and you should see something similar to:

    PEA starting...
	Options:
	  --help                      Help
	  --verbose                   More output
	  --quiet                     Less output
	  --config arg                Configuration file
	  --trace_level arg           Trace level
	  --antenna arg               ANTEX file
	  --navigation arg            Navigation file
	  --sinex arg                 SINEX file
	  --sp3file arg               Orbit (SP3) file
	  --clkfile arg               Clock (CLK) file
	  --dcbfile arg               Code Bias (DCB) file
	  --ionfile arg               Ionosphere (IONEX) file
	  --podfile arg               Orbits (POD) file
	  --blqfile arg               BLQ (Ocean loading) file
	  --erpfile arg               ERP file
	  --elevation_mask arg        Elevation Mask
	  --max_epochs arg            Maximum Epochs
	  --epoch_interval arg        Epoch Interval
	  --rnx arg                   RINEX station file
	  --root_input_dir arg        Directory containg the input data
	  --root_output_directory arg Output directory
	  --start_epoch arg           Start date/time
	  --end_epoch arg             Stop date/time
	  --dump-config-only          Dump the configuration and exit
	PEA finished

#### PEA Documentation

The documentation for the pea can be generated similarly using `doxygen` if it is installed.

	$ sudo apt-get install doxygen

	$ cd pea/cpp/build
	$ make doc_doxygen

The docs can then be found at `doc_doxygen/html/index.html`

### Ready

You are now ready to trial the examples in the `README.md` file and the examples in `Overview.pdf`



