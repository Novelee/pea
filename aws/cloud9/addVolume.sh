#!/bin/bash

# Specify the desired volume size in GiB as a command-line argument. If not specified, default to 250 GiB.
SIZE=${1:-250}

# Get the ID of the envrionment host Amazon EC2 instance.
INSTANCEID=$(curl http://169.254.169.254/latest/meta-data//instance-id)
EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
EC2_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed 's/[a-z]$//'`"

# Get the ID of the Amazon EBS volume associated with the instance.
VOLUMEID=$(aws ec2 create-volume --volume-type gp2 --size $SIZE --availability-zone $EC2_AVAIL_ZONE | jq -r .VolumeId)

sleep 3

# Wait for the resize to finish.
#while [ "$(aws ec2 describe-volumes-modifications --volume-id $VOLUMEID --filters Name=modification-state,Values="optimizing","completed" | jq '.VolumesModifications | length')" != "1" ]; do
#          sleep 1
#done

aws ec2 attach-volume --volume-id $VOLUMEID --instance-id $INSTANCEID --device /dev/sdf

sleep 5

mkfs.xfs /dev/nvme0n1

mkdir /data
mount /dev/nvme0n1 /data
chmod a+rw /data

# Add a line to fstab to ensure disk is mounted on reboot
echo "/dev/nvme0n1 /data xfs user,rw,noauto" >> /etc/fstab

# set up the mount script for the data drive
cp mount /etc/init.d/
chmod a+x /etc/init.d/mount

ln -s /etc/init.d/mount /etc/rc5.d/S99mount
ln -s /etc/init.d/mount /etc/rc4.d/K00mount
ln -s /etc/init.d/mount /etc/rc3.d/K00mount
ln -s /etc/init.d/mount /etc/rc2.d/K00mount

