
#include <map>
#include <string>

using std::string;
using std::map;

#include "ionoModel.hpp"
#include "algebra.hpp"
#include "common.hpp"
#include "testUtils.hpp"
#include "acsConfig.hpp"

/* Global parameters */
ionomodel_t iono_modl;
// int iono_model_reset;
// unsigned int ionex_map_index;
// FILE *iono_trace;

#define		INIT_VAR_RDCB	100.0
#define		INIT_VAR_SDCB	100.0
#define		INIT_VAR_SCHP	100.0

FILE* fp_iondebug;

extern int config_ionosph_model()
{
	fp_iondebug = nullptr;
	//fp_iondebug = fopen("iono_debug_trace.txt", "w");
	switch (acsConfig.ionFilterOpts.model)
	{
		case E_IonoModel::NONE:				    return 1;
		case E_IonoModel::SPHERICAL_HARMONICS:	return configure_iono_model_sphhar();
		case E_IonoModel::SPHERICAL_CAPS:	    return configure_iono_model_sphcap();
		case E_IonoModel::BSPLINE:			    return configure_iono_model_bsplin();
    }
    return 0;
}

static double ion_coef(int ind, Obs& obs, bool slant)
{
    switch(acsConfig.ionFilterOpts.model)
	{
		case E_IonoModel::SPHERICAL_HARMONICS:  return ion_coef_sphhar(ind, obs, slant);
		case E_IonoModel::SPHERICAL_CAPS:   	return ion_coef_sphcap(ind, obs, slant);
		case E_IonoModel::BSPLINE:      	  	return ion_coef_bsplin(ind, obs, slant);
    }
    return 0;
}

/*****************************************************************************************/
/* Updating the ionosphere model parameters                                              */
/* The ionosphere model should be initialized by calling 'config_ionosph_model'          */
/* Ionosphere measurments from stations should be loaded using 'update_station_measr'    */
/*****************************************************************************************/
void update_ionosph_model(
	Trace&			trace,
	StationList&	stations,       ///< []
	KFState&		kfState,
	double			tgap)        	///< []
{
    TestStack ts(__FUNCTION__);

	tracepde(3, trace,"UPDATE IONO MODEL ...\n");
	//count valid measurements for each station
	map<string, int> stationSatCount;
	for (auto& rec_ptr 	: stations)
	for (auto& obs 		: rec_ptr->obsList)
	{
		if (!obs.ionExclude)
		{
			stationSatCount[rec_ptr->id]++;
		}
	}

	//find pivot station with most sats on first epoch
	static Station* refRec = nullptr;
	if (refRec == nullptr)
	{
		int maxCount = 0;
		for (auto& rec_ptr : stations)
		{
			if(acsConfig.pivot_station	== rec_ptr->id)
			{
				refRec = rec_ptr;
				break;
			}

			int recCount = stationSatCount[rec_ptr->id];
			if (recCount > maxCount)
			{
				refRec = rec_ptr;
				maxCount = recCount;
			}
		}
	}
	tracepde(4, trace,"IONO REF STATION %s\n", refRec->id.c_str());

	//add measurements and create design matrix entries
	KFMeasEntryList kfMeasEntryList;

	for (auto& rec_ptr	: stations)                 //     for (sta=0,nv=0; sta<iono_model_nstations; sta++)
	for (auto& obs 		: rec_ptr->obsList)         //         for(s=0; s<meas->nmeas; s++)
	{
		auto& rec = *rec_ptr;

		if	( (stationSatCount[rec.id] < MIN_NSAT_STA)
			||(obs.ionExclude))
		{
			continue;
		}

		ObsKey obsKey;
		obsKey.Sat = obs.Sat;
		obsKey.str = rec.id;

		KFMeasEntry meas(&kfState, obsKey);

		meas.setValue(obs.STECsmth);
		meas.setNoise(obs.STECsmvr);


		/************ receiver DCB ************/        /* We may need to change this for multi-code solutions */
		KFKey recDCBKey;

		recDCBKey.type	= KF::DCB;
		recDCBKey.str	= rec.id;

		InitialState recDCBInit;

		recDCBInit.x = 0;
		recDCBInit.P = 0;
		recDCBInit.Q = 0;

		if (rec.id != refRec->id)
		{
			meas.addDsgnEntry(recDCBKey, 1, recDCBInit);
		}

		/************ satellite DCB ************/       /* We may need to change this for multi-code solutions */
		KFKey satDCBKey;

		satDCBKey.type	= KF::DCB;
		satDCBKey.Sat	= obs.Sat;

		InitialState satDCBInit;

		satDCBInit.x = 0;
		satDCBInit.P = 0;
		satDCBInit.Q = 0;

		meas.addDsgnEntry(satDCBKey, 1, satDCBInit);

		int obstweek;
		double obstsec = time2gpst(obs.time, &obstweek);

		/**** Ionosphere Model coefficients ****/
		for (int i = 0; i < acsConfig.ionFilterOpts.NBasis; i++)
		{
			KFKey ionModelKey;

			ionModelKey.type	= KF::IONOSPHERIC;
			ionModelKey.num		= i;

			InitialState ionModelInit;
			ionModelInit.x = 0;
			ionModelInit.P = 0;
			ionModelInit.Q = SQR(acsConfig.ionFilterOpts.model_noise);


			double coef = ion_coef(i, obs, true);
//			tracepde(2, trace,"ION_MODL %s %8.0f %3d %8.3f %8.3f %8.4f %8.4f %.5e\n",
			if(fp_iondebug)
				fprintf(fp_iondebug,"ION_MODL %s %8.0f %3d %9.5f %10.5f %8.5f %8.5f %12.5e %9.5f %12.5e\n",
        			((string)meas.obsKey).c_str(),
        			obstsec,
        			i,
        			obs.latIPP[0]*R2D,
        			obs.lonIPP[0]*R2D,
        			obs.angIPP[0],
        			obs.STECtoDELAY,
        			coef,
        			obs.STECsmth,
        			obs.STECsmvr);
			meas.addDsgnEntry(ionModelKey, coef, ionModelInit);
		}

		//add finalised entry
		kfMeasEntryList.push_back(meas);
	}

    //Process the measurements in a least-squares/kalman filter

	//add process noise to existing states as per their initialisations.
	kfState.stateTransition(trace, tgap);

	//combine the measurement list into a single design matrix, measurement vector, and measurement noise vector
	KFMeas combinedMeas = kfState.combineKFMeasList(kfMeasEntryList);

	//if there are uninitialised state values, estimate them using least squares
	if (kfState.lsqRequired)
	{
		kfState.lsqRequired = false;
		trace << "\r\n -------INITIALISING IONO USING LEAST SQUARES--------" << std::endl;

 		kfState.leastSquareInitStates(std::cout, combinedMeas, true);
	}
	else
	{
		//perform kalman filtering
		trace << "\r\n ------- DOING IONO KALMAN FILTER --------" << std::endl;

		kfState.filterKalman(trace, combinedMeas, false);
// 		trace << "\r\n ------- AFTER IONO KALMAN FILTER --------" << std::endl;
	}

	//trace << kfState.x;
    //trace << combinedMeas.A;
    //trace << kfState.P;

    MatrixXd atran = combinedMeas.A.transpose();
    TestStack::testMat("v", combinedMeas.V);
    TestStack::testMat("y", combinedMeas.Y);
    TestStack::testMat("x", kfState.x, 0, &kfState.P);
    TestStack::testMat("H", atran);
    TestStack::testMat("var", combinedMeas.R);
}
