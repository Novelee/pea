
#ifndef ACS_STREAM_H
#define ACS_STREAM_H

#include <iostream>
#include <sstream>
#include <utility>
#include <vector>
#include <string>
#include <thread>
#include <list>
#include <map>


using std::string;
using std::tuple;
using std::list;
using std::pair;
using std::map;






#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/asio.hpp>

namespace B_io		= boost::iostreams;
namespace B_asio	= boost::asio;

using boost::asio::ip::tcp;

#ifndef WIN32
#	include <dirent.h>
#	include <time.h>
#	include <sys/time.h>
#	include <sys/stat.h>
#	include <sys/types.h>
#endif

#include "observations.hpp"
#include "navigation.hpp"
#include "constants.h"
#include "common.hpp"
#include "gaTime.hpp"
#include "rinex.hpp"
#include "enum.h"
#include "rtcm.h"



//interfaces

/** Interface for streams that supply observations
 */
struct ObsStream
{
	sta_t	station = {};
	list<ObsList> obsListList;

	/** Return a list of observations from the stream.
	 * This function may be overridden by objects that use this interface
	 */
	virtual ObsList getObs()
	{
		if (obsListList.size() > 0)
		{
			ObsList& obsList = obsListList.front();

			std::sort(obsList.begin(), obsList.end(), [](Obs& a, Obs& b)
				{
					if (a.Sat.sys < b.Sat.sys)		return true;
					if (a.Sat.sys > b.Sat.sys)		return false;
					if (a.Sat.prn < b.Sat.prn)		return true;
					else							return false;
				});

			for (auto& obs					: obsList)
			for (auto& [ftype, sigsList]	: obs.SigsLists)
			{
				RawSig firstOfType = sigsList.front();
				obs.Sigs[ftype] = Sig(firstOfType);			//todo aaron, get any heirarchy of observations for double ups.
			}

			return obsList;
		}
		else
		{
			return ObsList();
		}
	}

	/** Return a list of observations from the stream, with a specified timestamp.
	 * This function may be overridden by objects that use this interface
	 */
	ObsList getObs(
		gtime_t	time,			///< Timestamp to get observations for
		double	delta = 0.5)	///< Acceptable tolerance around requested time
	{
		while (1)
		{
			ObsList obsList = getObs();

			if (obsList.size() == 0)
			{
				return obsList;
			}

			if (time == GTime::noTime())
			{
				return obsList;
			}

			if		(obsList.front().time < time - delta)
			{
				eatObs();
			}
			else if	(obsList.front().time > time + delta)
			{
				return ObsList();
			}
			else
			{
				return obsList;
			}
		}
	}


	/** Check to see if this stream has run out of data
	 */
	virtual bool isDead()
	{
		return false;
	}

	/** Remove some observations from memory
	 */
	void eatObs()
	{
		if (obsListList.size() > 0)
		{
			obsListList.pop_front();
		}
	}
};


/** Interface for objects that provide navigation data
 */
struct NavStream
{
	virtual void getNav()
	{

	}
};




#include "acsFileStream.hpp"
#include "acsRtcmStream.hpp"
#include "acsRinexStream.hpp"
#include "acsNtripStream.hpp"


/** Object that streams RTCM data from NTRIP castors.
 * Overrides interface functions
 */
struct NtripRtcmStream : NtripStream, RtcmStream
{
	NtripRtcmStream()
	{

	}

	NtripRtcmStream(const string& url_str)
	{
		url = URL::parse(url_str);
		open();
	}

	void setUrl(const string& url_str)
	{
		url = URL::parse(url_str);
	}

	void open()
	{
// 		connect();
	}

	ObsList getObs()
	{
		//get all data available from the ntrip stream and convert it to an iostream for processing
		getData();

		B_io::basic_array_source<char>					input_source(receivedData.data(), receivedData.size());
		B_io::stream<B_io::basic_array_source<char>>	inputStream(input_source);

		parseRTCM(inputStream);

		int pos = inputStream.tellg();
		if (pos < 0)
		{
			receivedData.clear();
		}
		else
		{
			receivedData.erase(receivedData.begin(), receivedData.begin() + pos);
		}

		//call the base function once it has been prepared
		ObsList obsList = ObsStream::getObs();
		return obsList;
	}
};

/** Object that streams RTCM data from a file.
 * Overrides interface functions
 */
struct FileRtcmStream : ACSFileStream, RtcmStream
{
	FileRtcmStream()
	{

	}

	FileRtcmStream(const string& path)
	{
		setPath(path);
		open();
	}

	void open()
	{
		openFile();
	}

	int lastObsListSize = -1;

	ObsList getObs()
	{
// 		getData();	not needed for files

		if (obsListList.size() < 3)
		{
			parseRTCM(inputStream);
		}

		//call the base function once it has been prepared
		ObsList obsList = ObsStream::getObs();

		lastObsListSize = obsList.size();
		return obsList;
	}

	bool isDead()
	{
		if	( (lastObsListSize != 0)
			||(inputStream))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
};

/** Object that streams RINEX data from a file.
 * Overrides interface functions
 */
struct FileRinexStream : ACSFileStream, RinexStream
{
	FileRinexStream()
	{

	}

	FileRinexStream(const string& path)
	{
		setPath(path);
		open();
	}

	void open()
	{
// 		openFile();
        fp = fopen(path.c_str(), "r");		//todo aaron, change these to rinex member functinos, and return streams.
		if (fp == nullptr)
		{
			BOOST_LOG_TRIVIAL(error) << "Error opening rinex file at " << path;
		}

		readRinexHeader();
	}

	int lastObsListSize = -1;

	ObsList getObs()
	{
// 		getData();	not needed for files

		if (obsListList.size() < 3)
		{
			parseRINEX(inputStream);
		}

		//call the base function once it has been prepared
		ObsList obsList = ObsStream::getObs();

		lastObsListSize = obsList.size();
		return obsList;
	}

	bool isDead()
	{
		if (lastObsListSize != 0)
		{
			return false;
		}

		if (feof(fp))
		{
			return true;
		}

		return false;
	}
};

// 		list<ACSNavStreamPtr> 	navstreams;
// 		auto acsStream_ptr1 = std::make_shared<_NtripRtcmStream>();		//todo aaron, do this to keep references to nav only streams
// 		navstreams.	push_back(acsStream_ptr2);


typedef std::shared_ptr<ObsStream> ACSObsStreamPtr;
typedef std::shared_ptr<NavStream> ACSNavStreamPtr;

#endif
