#include <sstream>
#include <iostream>
#include <memory>
#include <string>

using std::unique_ptr;
using std::string;

#include <boost/log/trivial.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#include <yaml-cpp/yaml.h>

#include "rtsSmoothing.hpp"
#include "streamTrace.hpp"
#include "acsConfig.hpp"
#include "acsStream.hpp"

#include "newsnx.hpp"

ACSConfig acsConfig = {};


/** Set value according to variable map entry if found
 */
template<typename TYPE>
void trySetValFromVM(
	boost::program_options::variables_map&	vm,		///< Variable map to search in
	string									key, 	///< Variable name
	TYPE&									output)	///< Destination to set
{
	if (vm.count(key))
	{
		output = vm[key].as<TYPE>();
	}
}

/** Check that filename is valid and the file exists
 */
bool checkValidFile(
	string&	path,			///< Filename to check
	string	description)	///< Description for error messages
{
	if	( !path.empty()
		&&!boost::filesystem::exists(path))
	{
		BOOST_LOG_TRIVIAL(error)
		<< "Invalid " << description << " file "
		<< path;

		return false;
	}
	return true;
}

bool checkValidFiles(
	vector<string>&	paths,
	string			description)
{
	bool pass = true;
	for (auto& path : paths)
	{
		pass &= checkValidFile(path, description);
	}
	return pass;
}

/** Remove any path from a fully qualified file
 */
void removePath(
	string &filepath)	// path_to_file
{
	size_t lastdirsep = filepath.rfind('/');

	if (lastdirsep == string::npos) return; // didn't find one ...

	filepath = filepath.substr(lastdirsep+1);

	return;
}

/** Add a root to paths that are not already absolutely defined
 */
void tryAddRootToPath(
	string& root,		///< Root path
	string& path)		///< Filename to prepend root path to
{
	if (path.empty())
	{
		return;
	}
	if (root[0] == '~')
	{
		string HOME = std::getenv("HOME");
		root.erase(0, 1);
		root.insert(0, HOME);
	}
	if (boost::filesystem::path(path).is_absolute())
	{
		return;
	}
	if (root.back() != '/')
	{
		root = root + '/';
	}
	path = root + path;

}

/** Add a root to paths that are not already absolutely defined
 */
void tryAddRootToPath(
	string&			root,		///< Root path
	vector<string>& paths)		///< Filename to prepend root path to
{
	for (auto& path : paths)
	{
		tryAddRootToPath(root, path);
	}
}

/** Create a station object from a file
 */
void ACSConfig::addStationFile(
	string fileName)			///< Filename to create station from
{
	tryAddRootToPath(root_stations_dir, fileName);
	boost::filesystem::path filePath(fileName);

	if (checkValidFile(fileName, "station") == false)
	{
		return;
	}

	if (!boost::filesystem::is_regular_file(filePath))
	{
		return;
	}

	auto filename = filePath.filename();
	string extension = filename.extension().string();

	//todo aaron check type another method too

// 	if	(   extension.length()		== 4
// 		&&( extension.substr(3)		== "O"
// 		  ||extension.substr(3)		== "o"
// 		  ||extension.substr(1,3)	== "rnx"
// 		  ||extension.substr(1,3)	== "RNX"))
	{
		// Assuming this is a RINEX file named as ssssdddf.yyO or ssssdddhmm.yyO where ssss is the station

		string stationId = filename.string().substr(0,4);
		boost::algorithm::to_upper(stationId);

		auto recOpts = getRecOpts(stationId);

		if (recOpts.exclude == false)
		{
			auto rinexStream_ptr = std::make_shared<FileRinexStream>(filePath.string());

			obsStreamMultimap.insert({stationId, std::move(rinexStream_ptr)});
			rinexFiles.push_back(filename.string());
		}
	}
}

/** Prepare the configuration of the program
 */
bool configure(
	int argc, 		///< Passthrough calling argument count
	char **argv)	///< Passthrough calling argument list
{
	// Command line options
	boost::program_options::options_description desc{"Options"};

	// Do not set default values here, as this will overide the configuration file opitions!!!
	desc.add_options()
	("help", 			"Help")
	("quiet", 			"Less output")
	("verbose", 		"More output")
	("very-verbose", 	"Much more output")
	("config", 					boost::program_options::value<std::string>(), 				"Configuration file")
	("trace_level", 			boost::program_options::value<int>(), 						"Trace level")
	("antenna", 				boost::program_options::value<std::string>(), 				"ANTEX file")
	("navigation", 				boost::program_options::value<std::string>(), 				"Navigation file")
	("sinex", 					boost::program_options::value<std::string>(), 				"SINEX file")
	("sp3file", 				boost::program_options::value<std::string>(), 				"Orbit (SP3) file")
	("clkfile", 				boost::program_options::value<std::string>(), 				"Clock (CLK) file")
	("dcbfile", 				boost::program_options::value<std::string>(), 				"Code Bias (DCB) file")
	("ionfile", 				boost::program_options::value<std::string>(), 				"Ionosphere (IONEX) file")
	("podfile", 				boost::program_options::value<std::string>(), 				"Orbits (POD) file")
// 	("grid", 					boost::program_options::value<std::string>(), 				"Grid file")
	("blqfile", 				boost::program_options::value<std::string>(), 				"BLQ (Ocean loading) file")
	("erpfile", 				boost::program_options::value<std::string>(), 				"ERP file")
// 	("receiver_clock_noise", 	boost::program_options::value<double>(), 					"Receiver Clock Noise")
// 	("satellite_clock_noise", 	boost::program_options::value<double>(), 					"Satellite Clock Noise")
	("elevation_mask", 			boost::program_options::value<float>(), 					"Elevation Mask")
	("max_epochs", 				boost::program_options::value<int>(), 						"Maximum Epochs")
	("epoch_interval", 			boost::program_options::value<float>(), 					"Epoch Interval")
	("rnx", 					boost::program_options::value<std::string>(),				"RINEX station file")
	("root_input_dir", 			boost::program_options::value<std::string>(),				"Directory containg the input data")
	("root_output_directory", 	boost::program_options::value<std::string>(),				"Output directory")
	("start_epoch", 			boost::program_options::value<std::string>(),	            "Start date/time")
	("end_epoch", 				boost::program_options::value<std::string>(),           	"Stop date/time")
	("run_rts_only", 			boost::program_options::value<std::string>(),				"RTS filename (without _xxxxx suffix)")
	("dump-config-only", "Dump the configuration and exit")
	;

	// ("end", boost::program_options::value<boost::posix_time::ptime>()->default_value(boost::posix_time::not_a_date_time), "Stop date/time")
	// see comment above

	boost::program_options::variables_map vm;

	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

	boost::program_options::notify(vm);

	if	( vm.count("help")
		||argc == 1)
	{
		BOOST_LOG_TRIVIAL(info) << desc;
		BOOST_LOG_TRIVIAL(info) << "PEA finished";

		exit(EXIT_SUCCESS);
	}

	if (vm.count("very-verbose"))	{	boost::log::core::get()->set_filter (boost::log::trivial::severity >= boost::log::trivial::debug);		}
	if (vm.count("verbose"))		{	boost::log::core::get()->set_filter (boost::log::trivial::severity >= boost::log::trivial::info);		}
	if (vm.count("quiet"))			{	boost::log::core::get()->set_filter (boost::log::trivial::severity >= boost::log::trivial::warning);	}

	if (vm.count("run_rts_only"))
	{
		string forward = vm["run_rts_only"].as<std::string>();

		BOOST_LOG_TRIVIAL(info)
		<< std::endl << "Processing RTS smoothing only for file: " << forward << "_xxxxxx";

		KFState kfState;
		kfState.rts_lag = -1;
		kfState.rts_forward_filename	= forward + "_forward";
		kfState.rts_reverse_filename	= forward + "_reverse";
		kfState.rts_smoothed_filename	= forward + "_smoothed";
		acsConfig.output_clocks			= true;
		acsConfig.clocks_filename		= forward + "_smoothed_clk";

		RTS_Process	(kfState, true);
		RTS_Output	(kfState);

		exit(EXIT_SUCCESS);
	}

	if (vm.count("config"))
	{
		string config = vm["config"].as<std::string>();

		BOOST_LOG_TRIVIAL(info)
		<< "Loading configuration from file " << config;

		bool pass = acsConfig.parse(config, vm);

		if (!pass)
		{
			BOOST_LOG_TRIVIAL(error)
			<< "Failed to parse configuration file " << config;

			return false;
		}
	}

	if (vm.count("rnx"))
	{
		acsConfig.addStationFile(vm["rnx"].as<std::string>());
	}

	// Dump the configuration information
	acsConfig.info(std::cout);

	// Check the configuration
	bool valid = true;
	valid &= checkValidFiles(acsConfig.snxfiles, 			"sinex file (snx file)");
	valid &= checkValidFiles(acsConfig.navfiles, 			"navfiles");
	valid &= checkValidFiles(acsConfig.sp3files, 			"orbit");
	valid &= checkValidFiles(acsConfig.clkfiles,			"clock file (CLK file)");
	valid &= checkValidFiles(acsConfig.snxfiles, 			"SINEX");
	valid &= checkValidFiles(acsConfig.blqfiles, 			"ocean loading information (Blq file)");
	valid &= checkValidFiles(acsConfig.erpfiles,			"earth rotation parameter file (ERP file)");
	valid &= checkValidFiles(acsConfig.dcbfiles,			"code Biases file (DCB file)");
	valid &= checkValidFiles(acsConfig.ionfiles,			"Ionosphere (IONEX file)");
	valid &= checkValidFiles(acsConfig.atxfiles, 			"antenna information (ANTEX file)");
	valid &= checkValidFile (acsConfig.tropOpts.gpt2grid,	"grid");

	if	( acsConfig.snxfiles.empty())
	{
		BOOST_LOG_TRIVIAL(error)
		<< "Invalid SINEX file ";

		return false;
	}

	if (vm.count("dump-config-only"))
	{
		BOOST_LOG_TRIVIAL(info)
		<< "PEA finished";

		exit(EXIT_SUCCESS);
	}

	return valid;
}

/** Print out the configuration data that has been read in.
 */
void ACSConfig::info(
	Trace& ss)		///< Trace file to output to
{
    ss << "\n";
    ss << "===============================\n";
    ss << "Configuration...\n";
    ss << "===============================\n";
	ss << "Inputs:\n";
    ss << "\tnavfiles:  "; for (auto& a : navfiles) ss << a << " "; ss << "\n";
    ss << "\tsnxfiles:  "; for (auto& a : snxfiles) ss << a << " "; ss << "\n";
    ss << "\tatxfiles:  "; for (auto& a : atxfiles) ss << a << " "; ss << "\n";
    ss << "\tdcbfiles:  "; for (auto& a : dcbfiles) ss << a << " "; ss << "\n";
    ss << "\tionfiles:  "; for (auto& a : ionfiles) ss << a << " "; ss << "\n";
    ss << "\tblqfiles:  "; for (auto& a : blqfiles) ss << a << " "; ss << "\n";
    ss << "\terpfiles:  "; for (auto& a : erpfiles) ss << a << " "; ss << "\n";
    ss << "\tsp3files:  "; for (auto& a : sp3files) ss << a << " "; ss << "\n";
	ss << "\torbfiles:  "; for (auto& a : orbfiles) ss << a << " "; ss << "\n";
	ss << "\tvmf3dir:   " << tropOpts.vmf3dir 			<< "\n";
	ss << "\torography: " << tropOpts.orography 		<< "\n";
	ss << "\tgrid:      " << tropOpts.gpt2grid 			<< "\n";
	ss << "\ttestfiles: " << testOpts.filename			<< "\n";
    ss << "\n";

	ss << "Outputs:\n";
	if (output_trace)		{	ss << "\tTrace level:        " << trace_level 				<< "\n"; }
	if (output_trace)		{	ss << "\ttrace_filename:     " << trace_filename 			<< "\n"; }
	if (output_residuals)	{	ss << "\tresiduals_filename: " << residuals_filename 		<< "\n"; }
	if (output_summary)		{	ss << "\tsummary_filename:   " << summary_filename 			<< "\n"; }
	if (output_clocks)		{	ss << "\tclocks_filename:    " << clocks_filename 			<< "\n"; }
	if (output_ionex)		{	ss << "\tionex_filename:     " << ionex_filename 			<< "\n"; }
	if (output_ionex)		{	ss << "\tiondsb_filename:    " << iondsb_filename 			<< "\n"; }
	if (output_iontrace)	{	ss << "\tiontrace_filename:  " << iontrace_filename 		<< "\n"; }
	if (output_plots)		{	ss << "\tplot_filename:      " << plot_filename 			<< "\n"; }

    ss << "\n";

    ss << "Process Modes:\n";
    ss << "\tUser:                " << process_user 				<< "\n";
    ss << "\tNetwork:             " << process_network 				<< "\n";
    ss << "\tMinimum Constraints: " << process_minimum_constraints 	<< "\n";
    ss << "\tInonsphereic:        " << process_ionosphere 			<< "\n";
    ss << "\tRTS Smoothing:       " << process_rts 					<< "\n";
    ss << "\tUnit tests:          " << process_tests				<< "\n";
    ss << "\n";

    ss << "Systems:\n";
    ss << "\tGPS:     " << process_sys[E_Sys::GPS] 		<< "\n";
    ss << "\tGLONASS: " << process_sys[E_Sys::GLO] 		<< "\n";
    ss << "\tGALILEO: " << process_sys[E_Sys::GAL] 		<< "\n";
    ss << "\tBEIDOU:  " << process_sys[E_Sys::CMP] 		<< "\n";
    ss << "\n";

    ss << "Elevation_mask: " << elevation_mask * R2D 	<< "\n";
    ss << "\n";

    ss << "Epochs:\n";
	if (max_epochs > 0)						{	ss << "\tmax_epochs:     " << max_epochs		<< "\n";    }
    if (!start_epoch.is_not_a_date_time())	{	ss << "\tepoch start:    " << start_epoch		<< "\n";    }
    if (!end_epoch.is_not_a_date_time())	{	ss << "\tepoch end:      " << end_epoch			<< "\n";    }
	if (epoch_interval > 0)					{	ss << "\tepoch_interval: " << epoch_interval	<< "\n";    }

    ss << "\n";
    ss << "Stations:\n";
    for (auto& filename : station_files)
	{
         ss << "\t" << root_stations_dir << filename << "\n";
	}

    ss << "\n";
    ss << "===============================\n";
    ss << "...End Configuration\n";
    ss << "===============================\n";
    ss << "\n";
}

/** Find a yaml node object using a list of strings
 */
YAML::Node stringsToYamlObject(
	YAML::Node		yamlBase, 			///< Yaml node to search within
	vector<string>	yamlNodeDescriptor)	///< List of strings of keys to trace hierarchy
{
	YAML::Node currentNode;
	YAML::Node nullNode;
	currentNode.reset(yamlBase);

	for (auto& desc : yamlNodeDescriptor)
	{
		auto test = currentNode[desc];
		if (!test)
		{
			return test;
		}

		currentNode.reset(currentNode[desc]);
	}
	return currentNode;
}

/** Set an output from yaml object if found
 */
template<typename TYPE>
bool trySetFromYaml(
	TYPE&			output,				///< Variable to output to
	YAML::Node&		yamlBase,			///< Yaml node to search within
	vector<string>	yamlNodeDescriptor)	///< List of strings of keys to trace hierarcy
{
	YAML::Node optNode = stringsToYamlObject(yamlBase, yamlNodeDescriptor);

	try
	{
		output = optNode.as<TYPE>();
		return true;
	}
	catch (...){}

	return false;
}

/** Set an output from command line options if found
 */
template<typename TYPE>
bool trySetFromOpts(
	TYPE&									output,			///< Variable to output to
	boost::program_options::variables_map&	commandOpts,	///< Command line object to search within
	vector<string>							nodeDescriptor)	///< List of strings of keys to trace hierarcy
{
	string name = nodeDescriptor.back();
	if (commandOpts.count(name))
	{
		try
		{
			output = commandOpts[name].as<TYPE>();
			return true;
		}
		catch (...) {}
	}
	return false;
}

/** Set an output from any config source if found
 */
template<typename TYPE>
bool trySetFromAny(
	TYPE&									output,				///< Variable to output to
	boost::program_options::variables_map&	commandOpts,		///< Command line object to search within
	YAML::Node&								yamlBase,			///< Yaml node to search within
	vector<string>							nodeDescriptor)		///< List of strings of keys to trace hierarcy
{
	bool found = false;
	found |= trySetFromYaml(output, yamlBase,	nodeDescriptor);
	found |= trySetFromOpts(output, commandOpts,nodeDescriptor);
	return found;
}

/** Set an enum from yaml, decoding strings to ints
 */
template <typename ENUM>
void trySetEnumOpt(
	int&			out,							///< Variable to output to
	YAML::Node&		yamlBase,						///< Yaml node to search within
	vector<string>	yamlNodeDescriptor,				///< List of strings of keys to trace hierarcy
	ENUM			(&_from_string)(const char*))	///< Function to decode enum strings
{
	YAML::Node optNode = stringsToYamlObject(yamlBase, yamlNodeDescriptor);

	try
	{
		out = _from_string(optNode.as<string>().c_str());
	}
	catch (...)	{}
}

/** Set the variables associated with kalman filter states from yaml
 */
void trySetKalmanFromYaml(
	KalmanModel&	output,	///< Variable to output to
	YAML::Node&		yaml,	///< Yaml node to search within
	string			key)	///< Key of yaml object
{
	auto entry = yaml[key];
	if (entry)
	{
		int proc_noise_dt = 1;
						trySetEnumOpt(proc_noise_dt,			entry, {"proc_noise_dt"	}, E_Period::_from_string_nocase);
						trySetFromYaml(output.estimate, 		entry, {"estimated"		});

						trySetFromYaml(output.sigma,			entry, {"sigma" 		});
						trySetFromYaml(output.clamp_max,		entry, {"clamp_max" 	});
						trySetFromYaml(output.clamp_min,		entry, {"clamp_min" 	});
						trySetFromYaml(output.apriori_val,		entry, {"apriori_val"	});
		bool found = 	trySetFromYaml(output.proc_noise,		entry, {"proc_noise" 	});
		if (found)
		{
			for (auto& proc : output.proc_noise)
			{
				proc /= proc_noise_dt;
			}
		}
	}
}

/** Set satellite options from yaml
 */
void getFromYaml(
	SatelliteOptions&	satOpts,			///< Satellite options variable to output to
	YAML::Node&			yamlBase,			///< Yaml node to search within
	vector<string>		yamlNodeDescriptor)	///< List of strings of keys of yaml hierarchy
{
	YAML::Node satNode = stringsToYamlObject(yamlBase, yamlNodeDescriptor);

	trySetKalmanFromYaml(satOpts.clk,		satNode, "clk"		);
	trySetKalmanFromYaml(satOpts.clk_rate,	satNode, "clk_rate"	);
	trySetKalmanFromYaml(satOpts.orb,		satNode, "orb"		);

	trySetFromYaml(satOpts.exclude, 	satNode, {"exclude"});

	satOpts._initialised = true;
}

/** Set receiver options from yaml
 */
void getFromYaml(
	StationOptions& 	recOpts, 			///< Receiver options variable to output to
	YAML::Node&			yamlBase,			///< Yaml node to search within
	vector<string>		yamlNodeDescriptor)	///< List of strings of keys of yaml hierarchy
{
	YAML::Node recNode = stringsToYamlObject(yamlBase, yamlNodeDescriptor);

	trySetKalmanFromYaml(recOpts.pos,			recNode, "pos"			);
	trySetKalmanFromYaml(recOpts.pos_rate,		recNode, "pos_rate"		);
	trySetKalmanFromYaml(recOpts.clk,			recNode, "clk"			);
	trySetKalmanFromYaml(recOpts.clk_rate,		recNode, "clk_rate"		);
	trySetKalmanFromYaml(recOpts.amb,			recNode, "amb"			);
	trySetKalmanFromYaml(recOpts.trop,			recNode, "trop"			);
	trySetKalmanFromYaml(recOpts.trop_grads,	recNode, "trop_grads"	);

	trySetEnumOpt	(recOpts.error_model,		recNode, {"error_model"	}, E_NoiseModel::_from_string_nocase);
	trySetFromYaml	(recOpts.code_sigmas,		recNode, {"code_sigmas"	});
	trySetFromYaml	(recOpts.phas_sigmas,		recNode, {"phase_sigmas"});
	trySetFromYaml	(recOpts.exclude, 			recNode, {"exclude"		});

	recOpts._initialised = true;
}

/** Set satellite options for a specific satellite using a hierarchy of sources
 */
SatelliteOptions& ACSConfig::getSatOpts(
	SatSys& Sat)	///< Satellite to search for options for
{
	SatelliteOptions& satOpts = satOptsMap[Sat.id()];

	//return early if possible
	if (satOpts._initialised)
		return satOpts;

	//initialise the options for this satellite
	SatelliteOptions& blockOpts = satOptsMap[Sat.blockType()];
	if (blockOpts._initialised == false)
	{
		//find it's parent
		SatelliteOptions& sysOpts = satOptsMap[Sat.sys._to_string()];
		if (sysOpts._initialised == false)
		{
			//find it's parent
			SatelliteOptions& globalOpts = satOptsMap["GLOBAL"];
			if (globalOpts._initialised == false)
			{
				//get specifics from config file
				getFromYaml(globalOpts, yaml, {"default_filter_parameters", "satellites"});
				globalOpts._initialised = true;
			}

			//initialise from its parent
			sysOpts = globalOpts;

			//get specifics from config file
			string sys = "SYS_" + Sat.sysName();
			getFromYaml(sysOpts, yaml, {"override_filter_parameters", "stations", sys});
		}

		//initialise from its parent
		blockOpts = sysOpts;

		//get specifics from config file
		string block = Sat.blockType();
		getFromYaml(blockOpts, yaml, {"override_filter_parameters", "stations", block});
	}

	//initialise from its parent
	satOpts = blockOpts;

	//get specifics from config file
	string prn = "PRN_" + Sat.id();
	string svn = "SVN_" + Sat.svn();
	getFromYaml(satOpts, yaml, {"override_filter_parameters", "stations", prn});
	getFromYaml(satOpts, yaml, {"override_filter_parameters", "stations", svn});

	return satOpts;
}

/** Set receiver options for a specific receiver using a hierarchy of sources
 */
StationOptions& ACSConfig::getRecOpts(
	string id)		///< Receiver to search for options for
{
	StationOptions& recOpts = recOptsMap[id];

	//return early if possible
	if (recOpts._initialised)
		return recOpts;

	//initialise the options for this reciever
	StationOptions& globalOpts = recOptsMap[""];
	if (globalOpts._initialised == false)
	{
		//get specifics from config file
		getFromYaml(globalOpts, yaml, {"default_filter_parameters", "stations"});
		globalOpts._initialised = true;
	}

	//initialise from its parent
	recOpts = globalOpts;

	//get specifics from config file
	getFromYaml(recOpts, yaml, {"override_filter_parameters", "stations", id});

	return recOpts;
}

/** Set minimum constraint options for a specific receiver using a hierarchy of sources
 */
MinimumStationOptions& ACSConfig::getMinConOpts(
	string id)			///< Receiver to search for options for
{
	MinimumStationOptions& recOpts = minCOpts.stationMap[id];

	//return early if possible
	if (recOpts._initialised)
		return recOpts;

	//initialise the options for this reciever
	MinimumStationOptions& globalOpts = minCOpts.stationMap[""];
	if (globalOpts._initialised == false)
	{
		//get specifics from config file
		trySetFromYaml(globalOpts.noise, yaml, {"minimum_constraints", "station_default_noise"});
		globalOpts._initialised = true;
	}

	//initialise from its parent
	recOpts = globalOpts;

	//get specifics from config file
	trySetFromYaml(recOpts.noise, yaml, {"minimum_constraints", "station_noise", id});

	return recOpts;
}

/** Set and scale a variable according to yaml options
 */
template<typename ENUM>
void trySetScaledFromYaml(
	double&			output,							///< Variable to output to
	YAML::Node		node,							///< Yaml node to search within
	vector<string>	number_parameter,				///< List of keys of the hierarchy to the value to be set
	vector<string>	scale_parameter,				///< List of keys of the hierarchy to the scale to be applied
	ENUM			(&_from_string)(const char*))	///< Function to decode scale enum strings
{
	double	number			= 0;
	int		number_units	= 1;
	trySetFromYaml(number,			node, number_parameter);
	trySetEnumOpt(number_units, node, scale_parameter, _from_string);
	number *= number_units;
	if (number != 0)
	{
		output = number;
	}
}

/** search for and replace section of string
 */
void replaceString(
	string&	str,			///< String to search within
	string	subStr, 		///< String to replace
	string	replacement)	///< Replacement string
{
	while (true)
	{
		size_t index = str.find(subStr);
		if (index == -1)
			break;

		str.erase	(index, subStr.size());
		str.insert(index, replacement);
	}
}

/** Replace macros for times with numeric values.
 * Available replacements are <DDD> <D <WWWW> <YYYY> <YY> <MM> <DD> <HH>
 */
void replaceTimes(
	string&						str,		///< String to replace macros within
	boost::posix_time::ptime	time_time)	///< Time to use for replacements
{
	string DDD	= "";
	string D	= "";
	string WWWW	= "";
	string YYYY	= "";
	string YY	= "";
	string MM	= "";
	string DD	= "";
	string HH	= "";

	if (!time_time.is_not_a_date_time())
	{
	// 	string gpsWeek0 = "2019-04-07 00:00:00.000";
		string gpsWeek0 = "1980-01-06 00:00:00.000";
		auto gpsZero = boost::posix_time::time_from_string(gpsWeek0);
		string time_string = boost::posix_time::to_iso_string(time_time);

		auto tm = to_tm(time_time);
		std::ostringstream ss;
		ss << std::setw(3) << std::setfill('0') << tm.tm_yday+1;
		string ddd = ss.str();

		auto gpsWeek = (time_time - gpsZero);
		int weeks = gpsWeek.hours() / 24 / 7;		//todo aaron, this is garbage
		ss.str("");
		ss << std::setw(4) << std::setfill('0') << weeks;
		string wwww = ss.str();

		DDD	= ddd;
		D	= std::to_string(tm.tm_wday);
		WWWW	= wwww;
		YYYY	= time_string.substr(0, 4);
		YY	= time_string.substr(2, 2);
		MM	= time_string.substr(4, 2);
		DD	= time_string.substr(6, 2);
		HH	= time_string.substr(9, 2);
	}

	replaceString(str, "<DDD>",		DDD);
	replaceString(str, "<D>",		D);
	replaceString(str, "<WWWW>",	WWWW);
	replaceString(str, "<YYYY>",	YYYY);
	replaceString(str, "<YY>",		YY);
	replaceString(str, "<MM>",		MM);
	replaceString(str, "<DD>",		DD);
	replaceString(str, "<HH>",		HH);
	replaceString(str, "<CONFIG>",	acsConfig.config_description);
}

void replaceTimes(
	vector<string>&				strs,
	boost::posix_time::ptime	time_time)
{
	for (auto& str : strs)
	{
		replaceTimes(str, time_time);
	}
}

/** Parse options to set acsConfig values.
 * Command line options will override any values set in config files, which will themselves override any program default values.
 */
bool ACSConfig::parse(
	string									filename,		///< Path to yaml based config file
	boost::program_options::variables_map&	commandOpts)	///< Variable map object of command line options
{
    try
    {
        yaml = YAML::LoadFile(filename);
    }
    catch (const YAML::BadFile &e)
	{
        BOOST_LOG_TRIVIAL(error) << "Failed to parse configuration file " << filename;
        BOOST_LOG_TRIVIAL(error) << e.msg;
        return false;
    }

    // General Files
	auto input_files = stringsToYamlObject(yaml, {"input_files"});
	{
		trySetFromAny(root_input_dir,	commandOpts, input_files, {"root_input_directory"	});
		trySetFromAny(atxfiles,			commandOpts, input_files, {"atxfiles"				});
		trySetFromAny(snxfiles,			commandOpts, input_files, {"snxfiles"				});
		trySetFromAny(blqfiles,			commandOpts, input_files, {"blqfiles"				});

		trySetFromAny(navfiles,			commandOpts, input_files, {"navfiles"			});
		trySetFromAny(sp3files,			commandOpts, input_files, {"sp3files"			});
		trySetFromAny(erpfiles,			commandOpts, input_files, {"erpfiles"			});
		trySetFromAny(dcbfiles,			commandOpts, input_files, {"dcbfiles"			});
		trySetFromAny(ionfiles,			commandOpts, input_files, {"ionfiles"			});
		trySetFromAny(clkfiles,			commandOpts, input_files, {"clkfiles"			});
		trySetFromAny(orbfiles,			commandOpts, input_files, {"orbfiles"			});

		tryAddRootToPath(root_input_dir, atxfiles);
		tryAddRootToPath(root_input_dir, snxfiles);
		tryAddRootToPath(root_input_dir, blqfiles);

		tryAddRootToPath(root_input_dir, navfiles);
		tryAddRootToPath(root_input_dir, orbfiles);
		tryAddRootToPath(root_input_dir, sp3files);
		tryAddRootToPath(root_input_dir, erpfiles);
		tryAddRootToPath(root_input_dir, dcbfiles);
		tryAddRootToPath(root_input_dir, ionfiles);
		tryAddRootToPath(root_input_dir, clkfiles);
	}

	trySetFromYaml(root_stations_dir,	yaml,				{"station_data", "root_stations_directory"	});
	trySetFromYaml(station_files,		yaml,				{"station_data", "files"					});

	tryAddRootToPath(root_stations_dir, root_stations_dir);

	string root_output_dir;
	auto output_files = stringsToYamlObject(yaml, 		{"output_files"});
	{
		trySetFromAny(trace_level,			commandOpts, output_files, {"trace_level"		});

		trySetFromYaml(root_output_dir,			output_files, {"root_output_directory"			});

		trySetEnumOpt( log_level, 				output_files, {"log_level" 			}, E_LogLevel::_from_string_nocase);
		trySetFromYaml(log_directory,			output_files, {"log_directory"		});
		trySetFromYaml(log_filename,			output_files, {"log_filename"		});

		trySetFromYaml(output_trace,			output_files, {"output_trace"		});
		trySetFromYaml(trace_directory,			output_files, {"trace_directory"	});
		trySetFromYaml(trace_filename,			output_files, {"trace_filename"		});

		trySetFromYaml(output_residuals,		output_files, {"output_residuals"	});
		trySetFromYaml(residuals_directory,		output_files, {"residuals_directory"});
		trySetFromYaml(residuals_filename,		output_files, {"residuals_filename"	});

		trySetFromYaml(output_summary,			output_files, {"output_summary"		});
		trySetFromYaml(summary_directory,		output_files, {"summary_directory"	});
		trySetFromYaml(summary_filename,		output_files, {"summary_filename"	});

		trySetFromYaml(output_clocks,			output_files, {"output_clocks"		});
		trySetFromYaml(clocks_directory,		output_files, {"clocks_directory"	});
		trySetFromYaml(clocks_filename,			output_files, {"clocks_filename"	});

		trySetFromYaml(output_ionex,			output_files, {"output_ionex"		});
		trySetFromYaml(ionex_directory,			output_files, {"ionex_directory"	});
		trySetFromYaml(ionex_filename,			output_files, {"ionex_filename"		});
		trySetFromYaml(iondsb_filename,			output_files, {"iondsb_filename"	});

		trySetFromYaml(output_iontrace,			output_files, {"output_iontrace"	});
		trySetFromYaml(iontrace_directory,		output_files, {"iontrace_directory"	});
		trySetFromYaml(iontrace_filename,		output_files, {"iontrace_filename"	});

		trySetFromYaml(output_plots,			output_files, {"output_plots"		});
		trySetFromYaml(plot_directory,			output_files, {"plot_directory"		});
		trySetFromYaml(plot_filename,			output_files, {"plot_filename"		});

		trySetFromYaml(output_sinex,			output_files, {"output_sinex"		});
		trySetFromYaml(sinex_directory,			output_files, {"sinex_directory"	});
		trySetFromYaml(sinex_filename,			output_files, {"sinex_filename"		});

		trySetFromYaml(output_persistance,		output_files, {"output_persistance"		});
		trySetFromYaml(persistance_directory,	output_files, {"persistance_directory"	});
		trySetFromYaml(persistance_filename,	output_files, {"persistance_filename"	});

		trySetScaledFromYaml(log_rotate_period, 		output_files, {"log_rotate_period"		}, {"log_rotate_period_units"		}, E_Period::_from_string_nocase);
		trySetScaledFromYaml(trace_rotate_period,		output_files, {"trace_rotate_period"	}, {"trace_rotate_period_units"		}, E_Period::_from_string_nocase);
		trySetScaledFromYaml(residuals_rotate_period,	output_files, {"residuals_rotate_period"}, {"residuals_rotate_period_units"	}, E_Period::_from_string_nocase);
		trySetScaledFromYaml(summary_rotate_period,		output_files, {"summary_rotate_period"	}, {"summary_rotate_period_units"	}, E_Period::_from_string_nocase);
		trySetScaledFromYaml(config_rotate_period,		output_files, {"config_rotate_period"	}, {"config_rotate_period_units"	}, E_Period::_from_string_nocase);
		trySetScaledFromYaml(ionex_rotate_period,		output_files, {"ionex_rotate_period"	}, {"ionex_rotate_period_units"		}, E_Period::_from_string_nocase);
		trySetScaledFromYaml(iontrace_rotate_period,	output_files, {"iontrace_rotate_period"	}, {"iontrace_rotate_period_units"	}, E_Period::_from_string_nocase);
	}

	auto processing_options = stringsToYamlObject(yaml, {"processing_options"});
	{
		trySetFromAny(max_epochs,		commandOpts, processing_options, {"max_epochs"		});
		trySetFromAny(epoch_interval,	commandOpts, processing_options, {"epoch_interval"	});

		string startStr;
		string stopStr;
		//trySetFromYaml(startStr,		processing_options, {"start_epoch"		});
		trySetFromAny(startStr,		    commandOpts, processing_options, {"start_epoch"		});
		trySetFromAny(stopStr,          commandOpts, processing_options, {"end_epoch"		});
		//trySetFromYaml(stopStr,			processing_options, {"end_epoch"		});
		if (!startStr.empty())	start_epoch	= boost::posix_time::time_from_string(startStr);
		if (!stopStr .empty())	end_epoch	= boost::posix_time::time_from_string(stopStr);

		trySetFromYaml(process_minimum_constraints,	processing_options, {"process_modes","minimum_constraints"	});
		trySetFromYaml(process_network,				processing_options, {"process_modes","network"				});
		trySetFromYaml(process_user,				processing_options, {"process_modes","user"					});
		trySetFromYaml(process_ionosphere,			processing_options, {"process_modes","ionosphere"			});
		trySetFromYaml(process_rts,					processing_options, {"process_modes","rts"					});
		trySetFromYaml(process_tests,				processing_options, {"process_modes","unit_tests"			});

		for (int i = 0; i < E_Sys::_size(); i++)
		{
			int		index	= E_Sys::_values()[i];
			string	sys		= E_Sys::_names() [i];		boost::algorithm::to_lower(sys);

			trySetFromYaml(process_sys[index],	processing_options, {"process_sys", sys	});
		}

		bool found = trySetFromAny(elevation_mask,	commandOpts, processing_options, {"elevation_mask"	});
		if (found)
			elevation_mask *= D2R;

		trySetEnumOpt( ppp_ephemeris, 		processing_options,	{"ppp_ephemeris" 				}, E_Ephemeris::_from_string_nocase);

		trySetFromYaml(tide_solid,			processing_options, {"tide_solid"						});
		trySetFromYaml(tide_otl,			processing_options, {"tide_otl"							});
		trySetFromYaml(tide_pole,			processing_options, {"tide_pole"						});

		trySetFromYaml(phase_windup,		processing_options, {"phase_windup"						});
		trySetFromYaml(reject_eclipse,		processing_options, {"reject_eclipse"					});
		trySetFromYaml(raim,				processing_options, {"raim"								});
		trySetFromYaml(thres_slip,			processing_options, {"cycle_slip",		"thres_slip"	});
		trySetFromYaml(max_inno,			processing_options, {"max_inno"			});
		trySetFromYaml(deweight_factor,		processing_options, {"deweight_factor"	});
		trySetFromYaml(max_gdop,			processing_options, {"max_gdop"			});
		trySetFromYaml(antexacs,			processing_options, {"antexacs"			});

		trySetFromYaml(pivot_station,		processing_options, {"pivot_station"	});
		trySetFromYaml(pivot_satellite,		processing_options, {"pivot_satellite"	});

		trySetFromYaml(wait_next_epoch,		processing_options, {"wait_next_epoch"	});
		trySetFromYaml(wait_all_stations,	processing_options, {"wait_all_stations"});

		trySetFromYaml(debug_cs,			processing_options, {"debug_cs"			});
		trySetFromYaml(debug_lom,			processing_options, {"debug_lom"		});
		trySetFromYaml(debug_csacc,			processing_options, {"debug_csacc"		});
	}

	auto user_filter = stringsToYamlObject(yaml, {"user_filter_parameters"});
	{
		trySetEnumOpt( pppOpts.inverter, 				user_filter,	{"inverter" 				}, E_Inverter::_from_string_nocase);
		trySetFromYaml(pppOpts.max_filter_iter,			user_filter,	{"max_filter_iterations"	});
		trySetFromYaml(pppOpts.max_prefit_remv,			user_filter,	{"max_prefit_remvovals"		});
		trySetFromYaml(pppOpts.rts_lag,					user_filter,	{"rts_lag"					});
		trySetFromYaml(pppOpts.rts_directory,			user_filter,	{"rts_directory"			});
		trySetFromYaml(pppOpts.rts_filename,			user_filter,	{"rts_filename"				});
		trySetFromYaml(pppOpts.ballistics,				user_filter,	{"ballistics"				});
		trySetFromYaml(pppOpts.outage_reset_count,		user_filter,	{"outage_reset_count"		});
		trySetFromYaml(pppOpts.phase_reject_count,		user_filter,	{"phase_reject_count"		});
	}

	auto network_filter = stringsToYamlObject(yaml, {"network_filter_parameters"});
	{
		trySetEnumOpt( netwOpts.inverter, 			network_filter,	{"inverter" 				}, E_Inverter::_from_string_nocase);
		trySetEnumOpt( netwOpts.filter_mode, 		network_filter, {"process_mode" 			}, E_FilterMode::_from_string_nocase);
		trySetFromYaml(netwOpts.max_filter_iter,	network_filter, {"max_filter_iterations"	});
		trySetFromYaml(netwOpts.max_prefit_remv,	network_filter, {"max_prefit_remvovals"		});
		trySetFromYaml(netwOpts.rts_lag,			network_filter,	{"rts_lag"					});
		trySetFromYaml(netwOpts.rts_directory,		network_filter,	{"rts_directory"			});
		trySetFromYaml(netwOpts.rts_filename,		network_filter,	{"rts_filename"				});

	}

	auto troposphere = stringsToYamlObject(processing_options, {"troposphere"});
	{
		trySetEnumOpt( tropOpts.model, 		troposphere, {"model" 		}, E_TropModel::_from_string_nocase);
		trySetFromYaml(tropOpts.vmf3dir,	troposphere, {"vmf3dir"		});
		trySetFromYaml(tropOpts.orography,	troposphere, {"orography"	});
		trySetFromYaml(tropOpts.gpt2grid,	troposphere, {"gpt2grid"	});

		tryAddRootToPath(root_input_dir, tropOpts.vmf3dir);
		tryAddRootToPath(root_input_dir, tropOpts.orography);
		tryAddRootToPath(root_input_dir, tropOpts.gpt2grid);
	}

	auto ionosphere = stringsToYamlObject(processing_options, {"ionosphere"});
	{
		trySetEnumOpt( ionoOpts.corr_mode, 	ionosphere, {"corr_mode" 		}, E_IonoMode::_from_string_nocase);
		trySetEnumOpt( ionoOpts.iflc_freqs,	ionosphere, {"iflc_freqs" 		}, E_LinearCombo::_from_string_nocase);
	}

	auto unit_tests = stringsToYamlObject(yaml, {"unit_test_options"});
	{
		trySetFromYaml(testOpts.stop_on_fail,	unit_tests, {"stop_on_fail"		});
		trySetFromYaml(testOpts.stop_on_done,	unit_tests, {"stop_on_done"		});
		trySetFromYaml(testOpts.output_errors,	unit_tests, {"output_errors"	});
		trySetFromYaml(testOpts.absorb_errors,	unit_tests, {"absorb_errors"	});
		trySetFromYaml(testOpts.directory,		unit_tests, {"directory"		});
		trySetFromYaml(testOpts.filename,		unit_tests, {"filename"			});

		tryAddRootToPath(root_input_dir, 	testOpts.directory);
		tryAddRootToPath(testOpts.filename, testOpts.filename);
	}

	auto ionFilter = stringsToYamlObject(yaml, {"ionosphere_filter_parameters"});
	{
		trySetEnumOpt( ionFilterOpts.model, 			ionFilter, {"model" 				}, E_IonoModel::_from_string_nocase);
		trySetFromYaml(ionFilterOpts.lat_center,		ionFilter, {"lat_center"			});
		trySetFromYaml(ionFilterOpts.lon_center,		ionFilter, {"lon_center"			});
		trySetFromYaml(ionFilterOpts.lat_width,			ionFilter, {"lat_width"				});
		trySetFromYaml(ionFilterOpts.lon_width,			ionFilter, {"lon_width"				});
		trySetFromYaml(ionFilterOpts.lat_res,			ionFilter, {"lat_res"				});
		trySetFromYaml(ionFilterOpts.lon_res,			ionFilter, {"lon_res"				});
		trySetFromYaml(ionFilterOpts.time_res,			ionFilter, {"time_res"				});
		trySetFromYaml(ionFilterOpts.func_order,		ionFilter, {"func_order"			});
		trySetFromYaml(ionFilterOpts.layer_heights,		ionFilter, {"layer_heights"			});
		trySetFromYaml(ionFilterOpts.model_noise,		ionFilter, {"model_noise"			});
		trySetFromYaml(ionFilterOpts.max_filter_iter,	ionFilter, {"max_filter_iterations"	});
		trySetFromYaml(ionFilterOpts.max_prefit_remv,	ionFilter, {"max_filter_removals"	});
		trySetFromYaml(ionFilterOpts.rts_lag,			ionFilter, {"rts_lag"				});
		trySetFromYaml(ionFilterOpts.rts_directory,		ionFilter, {"rts_directory"			});
		trySetFromYaml(ionFilterOpts.rts_filename,		ionFilter, {"rts_filename"			});

		trySetKalmanFromYaml(ionFilterOpts.ion,			ionFilter, "ion");

		for (auto& a : ionFilterOpts.layer_heights)
		{
			a *= 1000; //km to m
		}
	}

	auto minCon = stringsToYamlObject(yaml, {"minimum_constraints"});
	{
		trySetEnumOpt( minCOpts.filter_mode, 			minCon, {"process_mode" 			}, E_FilterMode::_from_string_nocase);
		trySetFromYaml(minCOpts.estimate_scale,			minCon, {"estimate", "scale"		});
		trySetFromYaml(minCOpts.estimate_rotation,		minCon, {"estimate", "rotation"		});
		trySetFromYaml(minCOpts.estimate_translation,	minCon, {"estimate", "translation"	});
	}

	auto outputOptions = stringsToYamlObject(yaml, {"output_options"});
	{
		trySetFromYaml(config_description,	outputOptions, {"config_description"});
		trySetFromYaml(analysis_agency,		outputOptions, {"analysis_agency"	});
		trySetFromYaml(analysis_center,		outputOptions, {"analysis_center"	});
		trySetFromYaml(analysis_program,	outputOptions, {"analysis_program"	});
		trySetFromYaml(rinex_comment,		outputOptions, {"rinex_comment"		});
	}


	auto defaultFilter = stringsToYamlObject(yaml, {"default_filter_parameters"});
	{
		trySetKalmanFromYaml(netwOpts.eop,			defaultFilter, "eop");
	}

	//todo get layer heights and process noise

	//todo get observation errors

	//todo get observation hierarchy

	auto troposhpere = stringsToYamlObject(processing_options, {"troposhpere"});
	{
		trySetEnumOpt( tropOpts.model, 			troposhpere,	{"model" 			}, E_TropModel::_from_string_nocase);
		trySetFromYaml(tropOpts.gpt2grid,		troposhpere,	{"gpt2grid"			});
		trySetFromYaml(tropOpts.vmf3dir,		troposhpere,	{"vmf3dir"			});
		trySetFromYaml(tropOpts.orography,		troposhpere,	{"orography"		});
	}

	tryAddRootToPath(root_output_dir, 				trace_directory);
	tryAddRootToPath(root_output_dir,				log_directory);
	tryAddRootToPath(root_output_dir,				residuals_directory);
	tryAddRootToPath(root_output_dir,				summary_directory);
	tryAddRootToPath(root_output_dir,				clocks_directory);
	tryAddRootToPath(root_output_dir,				ionex_directory);
	tryAddRootToPath(root_output_dir,				ionex_directory);
	tryAddRootToPath(root_output_dir,				iontrace_directory);
	tryAddRootToPath(root_output_dir,				plot_directory);
	tryAddRootToPath(root_output_dir,				sinex_directory);
	tryAddRootToPath(root_output_dir,				persistance_directory);
	tryAddRootToPath(root_output_dir,				ionFilterOpts.rts_directory);
	tryAddRootToPath(root_output_dir,				netwOpts.rts_directory);
	tryAddRootToPath(root_output_dir,				pppOpts.rts_directory);

	tryAddRootToPath(trace_directory, 				trace_filename);
	tryAddRootToPath(log_directory,					log_filename);
	tryAddRootToPath(residuals_directory,			residuals_filename);
	tryAddRootToPath(summary_directory,				summary_filename);
	tryAddRootToPath(clocks_directory,				clocks_filename);
	tryAddRootToPath(ionex_directory,				ionex_filename);
	tryAddRootToPath(ionex_directory,				iondsb_filename);
	tryAddRootToPath(iontrace_directory,			iontrace_filename);
	tryAddRootToPath(plot_directory,				plot_filename);
	tryAddRootToPath(sinex_directory,				sinex_filename);
	tryAddRootToPath(persistance_directory,			persistance_filename);
	tryAddRootToPath(ionFilterOpts.rts_directory,	ionFilterOpts.rts_filename);
	tryAddRootToPath(netwOpts.rts_directory,		netwOpts.rts_filename);
	tryAddRootToPath(pppOpts.rts_directory,			pppOpts.rts_filename);

	//Try to change all filenames to replace <YYYY> etc with other values.
	replaceTimes(navfiles,				start_epoch);
	replaceTimes(orbfiles,				start_epoch);
	replaceTimes(sp3files,				start_epoch);
	replaceTimes(clkfiles,				start_epoch);
	replaceTimes(atxfiles,				start_epoch);
	replaceTimes(snxfiles,				start_epoch);
	replaceTimes(blqfiles,				start_epoch);
	replaceTimes(erpfiles,				start_epoch);
	replaceTimes(dcbfiles,				start_epoch);
	replaceTimes(ionfiles,				start_epoch);
	replaceTimes(log_directory,			start_epoch);
	replaceTimes(log_filename,			start_epoch);
	replaceTimes(trace_directory,		start_epoch);
	replaceTimes(trace_filename,		start_epoch);
	replaceTimes(residuals_directory,	start_epoch);
	replaceTimes(residuals_filename,	start_epoch);
	replaceTimes(ionex_directory,		start_epoch);
	replaceTimes(ionex_filename,		start_epoch);
	replaceTimes(iondsb_filename,		start_epoch);
	replaceTimes(iontrace_directory,	start_epoch);
	replaceTimes(iontrace_filename,		start_epoch);
	replaceTimes(summary_directory,		start_epoch);
	replaceTimes(summary_filename,		start_epoch);
	replaceTimes(clocks_directory,		start_epoch);
	replaceTimes(clocks_filename,		start_epoch);
	replaceTimes(root_stations_dir,		start_epoch);
	replaceTimes(plot_directory,		start_epoch);
	replaceTimes(plot_filename,			start_epoch);
	replaceTimes(sinex_filename,		start_epoch);
	replaceTimes(persistance_filename,			start_epoch);
	replaceTimes(pppOpts.rts_filename,			start_epoch);
	replaceTimes(netwOpts.rts_filename,			start_epoch);
	replaceTimes(ionFilterOpts.rts_filename,	start_epoch);

	for (auto& station_file : station_files)
	{
		replaceTimes(station_file,		start_epoch);
	}


	string streamRoot = "";
	trySetFromYaml(streamRoot,		yaml,	{"station_data", "stream_root"	});

	auto streamNode = stringsToYamlObject(yaml, {"station_data","streams"});


	map<string, string> ntripStreams;
	for (auto streamUrl : streamNode)
	{
 		string streamUrl_str = streamUrl.as<string>();
		string fullUrl = streamUrl_str;

// 		if (filename_str == "<ALL MOUNTPOINTS>")

		tryAddRootToPath(streamRoot, fullUrl);

		string id = streamUrl_str.substr(0,4);

		ntripStreams[id] = fullUrl;
	}

	for (auto& [id, streamUrl] : ntripStreams)
	{
		auto ntripStream_ptr = std::make_shared<NtripRtcmStream>(streamUrl);

		obsStreamMultimap.insert({id, std::move(ntripStream_ptr)});
	}

    boost::filesystem::path root_stations_path(root_stations_dir);

	if (!boost::filesystem::is_directory(root_stations_path))
	{
		BOOST_LOG_TRIVIAL(error)
		<< "Invalid input directory "
		<< root_stations_dir;
	}
	else
	{
		list<string> stationFiles;

		auto fileNode = stringsToYamlObject(yaml, {"station_data", "files"});
		for (auto filename : fileNode)
		{
			string filename_str = filename.as<string>();
			if (filename_str == "<SEARCH_DIRECTORY>")
			{
				BOOST_LOG_TRIVIAL(info)
				<< "Looking for station observation data in directory "
				<< root_stations_dir << "...";

				for (auto dir_file : boost::filesystem::directory_iterator(root_stations_path))
				{
					if (boost::filesystem::is_regular_file(dir_file))
					{
						auto filestr = dir_file.path().filename().string();
						tryAddRootToPath(root_stations_dir, filestr);
						stationFiles.push_back(filestr);
					}
				}
				continue;
			}

			tryAddRootToPath(root_stations_dir, filename_str);
			stationFiles.push_back(filename_str);
		}

		for (auto& fileName : stationFiles)
		{
			addStationFile(fileName);
		}

		if (obsStreamMultimap.empty())
		{
			BOOST_LOG_TRIVIAL(error)
			<< "No station files found in "
			<< root_stations_dir;
		}
	}

    return true;
}
