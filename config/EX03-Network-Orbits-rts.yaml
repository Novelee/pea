# Ex03 - Network + Clock + Orbits Estimation Example

input_files:

    root_input_directory: /data/acs/pea/proc/exs/products/

    atxfiles:   [ igs14_2045_plus.atx                                ]    # required
    snxfiles:   [ igs19P2062.snx                                     ]    # required
    blqfiles:   [ OLOAD_GO.BLQ                                       ]    # required if ocean loading is applied
    navfiles:   [ brdm1990.19p                                       ]    # required but shouldn't!
    sp3files:   [ igs20624.sp3                                       ]
    erpfiles:   [ igs19P2062.erp                                     ]    #config parser doesn't accept weekly files yet.
    dcbfiles:   [ CAS0MGXRAP_20191990000_01D_01D_DCB.BSX             ]    # monthly DCB file
    #clkfiles:   [     jpl20624.clk                                   ]   # Clk file
    orbfiles:   [ orb_partials/gag20624_orbits_partials.out.ecom2      ]    # only need this if we are estimating orbits

station_data:

    root_stations_directory: /data/acs/pea/proc/exs/data

    files:
        - <SEARCH_DIRECTORY>                           #- searching all in file_root directory

output_files:

    root_output_directory:          /data/acs/pea/output/exs/<CONFIG>/

    log_level:                      warn                             #debug, info, warn, error as defined in boost::log
    log_directory:                  ./
    log_filename:                   <CONFIG>-<YYYY><DDD><HH>.LOG

    output_trace:                   true
    trace_level:                    3
    trace_directory:                ./
    trace_filename:                 <CONFIG>-<STATION><YYYY><DDD><HH>.TRACE

    output_residuals:               true
    residuals_directory:            ./
    residuals_filename:             <CONFIG>-<YYYY><DDD><HH>.RES

    output_summary:                 true
    summary_directory:              ./
    summary_filename:               PEA<YYYY><DDD><HH>.SUM

    output_ionex:                   false
    ionex_directory:                ./
    ionex_filename:                 IONEX.ionex
    iondsb_filename:                IONEX.iondcb

    output_clocks:                  true
    clocks_directory:               ./
    clocks_filename:                <CONFIG>.clk

    output_sinex:                   true

output_options:

    config_description:             EX03
    analysis_agency:                GAA
    analysis_center:                Geoscience Australia
    analysis_program:               AUSACS
    rinex_comment:                  AUSNETWORK1

processing_options:

    start_epoch:                2019-07-18 00:00:00
    end_epoch:                  2019-07-18 23:59:30
    max_epochs:                 0        #0 is infinite
    epoch_interval:             30          #seconds

    process_modes:
        user:                   false
        network:                true
        minimum_constraints:    false
        rts:                    true
        ionosphere:             false
        unit_tests:             false

    process_sys:
        gps:            true
        #glo:           true
        #gal:           true
        #bds:           true

    elevation_mask:     5   #degrees

    tide_solid:         true
    tide_pole:          true
    tide_otl:           true

    phase_windup:       true
    reject_eclipse:     true            #  reject observation during satellite eclipse periods
    raim:               true
    antexacs:           true

    cycle_slip:
        thres_slip: 0.05

    max_inno:   0
    max_gdop:   30

    troposphere:
        model:      vmf3    #gpt2
        vmf3dir:    grid5/
        orography:  orography_ell_5x5

    ionosphere:
        corr_mode:      iono_free_linear_combo
        iflc_freqs:     l1l2_only   #any l1l2_only l1l5_only

    pivot_station:        "USN7"
    #pivot_satellite:      "G01"

network_filter_parameters:

    process_mode:               kalman      #lsq
    inverter:                   LLT         #LLT LDLT INV

    max_filter_iterations:      2
    max_filter_removals:        3

    rts_lag:                    -1      #-ve for full reverse, +ve for limited epochs
    rts_directory:              ./
    rts_filename:               <CONFIG>-Orbits.rts

default_filter_parameters:

    stations:

        error_model:        elevation_dependent         #uniform elevation_dependent
        code_sigmas:        [0.15]
        phase_sigmas:       [0.0015]

        pos:
            estimated:          false
            sigma:              [0.5]
            proc_noise:         [0]
            #apriori:                                   # taken from other source, rinex file etc.
            #frame:              xyz #ned
            #proc_noise_model:   Gaussian
            #clamp_max:          [+0.5]
            #clamp_min:          [-0.5]

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [1.8257418583505538]
            #proc_noise_model:   Gaussian

        clk_rate:
            estimated:          false
            sigma:              [10]
            proc_noise:         [1e-3]
            #clamp_max:          [1000]
            #clamp_min:          [-1000]

        amb:
            estimated:          true
            sigma:              [60]
            proc_noise:         [0]

        trop:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0.000083333]

        trop_grads:
            estimated:          true 
            sigma:              [0.1]
            proc_noise:         [0.0000036]

    satellites:

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [0.03651483716701108]

        clk_rate:
            estimated:          false
            sigma:              [1]
            proc_noise:         [1e-6]
            #clamp_max:          [+500]
            #clamp_min:          [-500]

        orb:
            estimated:          true
            sigma:              [5e-1]

    eop:
        estimated:  true
        sigma:      [10]


override_filter_parameters:

    stations:

        #ALICAUS01:
            #pos:
                #sigma:              [0.001]
                #proc_noise:         [0]
                ##proc_noise_model:   Gaussian
            #clk:
                #sigma:              [0.01]

        AGGO:
            exclude: true
        #ALIC:
            #exclude: true
        #ANKR:
            #exclude: true
                #network_estimated:  false

    satellites:

        ### Constellation Overrides
        #SYS_GPS:
            #srp:
                #sigma:              [0.01]
                #proc_noise:         [0.001]
                ##proc_noise_model:   RandomWalk

        #SYS_GAL:
            #clk:
                #sigma:              [1.0]
                #proc_noise:         [0.01]
                ##proc_noise_model:   RandomWalk

        #### Block Type Overrides
        #GPS-IIR-A:
            #pos: { sigma: [10.0] }
            #vel: { sigma: [0.1] }

        ### PRN Overrides
        #PRN_G10:
            #pos: { sigma: [10.0]    }
            #vel: { sigma: [0.1]     }
            #clk: { sigma: [1.0]     }

        #PRN_G15:
            #exclude: true

        ### SVN Overrides
        #SVN_G265:
            #pos: {sigma: [10.0] }
            #vel: {sigma: [0.1]  }


minimum_constraints:

    process_mode:       lsq     #kalman

    estimate:
        translation:    true
        rotation:       true
        scale:          true   #not yet implemented

    #station_default_noise: -1        #constrain none by default (negative numbers are not constrained)
    station_default_noise: +1       #constrain all by default

    station_noise:
        ALIC: 1     #constrain weakly
        ACSG: 1     #constrain weakly
        AREG: 1     #constrain weakly
        BAKR: 1     #constrain weakly
        BAKO: 1     #constrain weakly
        BREW: 1     #constrain weakly
        CAS1: 1     #constrain weakly
        DGAR: 1     #constrain weakly
        FAIR: 1     #constrain weakly
        HERS: 1     #constrain weakly
        IISC: 1     #constrain weakly
        JPLM: 1     #constrain weakly
        KZN2: 1     #constrain weakly
        LAUT: 1     #constrain weakly
        MATE: 1     #constrain weakly
        NKLG: 1     #constrain weakly
        STJ3: 1     #constrain weakly
        THGT: 1     #constrain weakly
        USN7: 1     #constrain weakly
        USUD: 1     #constrain weakly
        YEL2: 1     #constrain weakly
        ZAMB: 1     #constrain weakly
